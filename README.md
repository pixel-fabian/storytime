# Storytime 🎮

As you sit around the campfire, it's your turn to tell an exciting story. Do you remember when the mighty dragon attacked the lands?

Time is of the essence. You had only seconds to equip yourself. Did you find the right equipment to defeat the dragon once and for all?

A game by: lupercalia & pixel-fabian

Created during [20 Second Game Jam](https://itch.io/jam/20-second-game-jam)

## Play

> Play on [itch.io](https://pixel-fabian.itch.io/storytime)

### Controls

- WASD or arrow keys to move

## Tools

- Game engine: [Phaser](https://phaser.io/)
- Sprite editor: [aseprite](https://www.aseprite.org/)
- SFX generator: [jsfxr](https://sfxr.me/)
- Song maker: [Chrome music lab](https://musiclab.chromeexperiments.com/Song-Maker/)

## Licence

### Code

Storytime - A browser game - Copyright (C) 2022 pixel-fabian & lupercalia

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.

### Assets

- Sprites, music and sound effects: pixel-fabian & lupercalia CC-BY-SA 4.0
- Some sound effects from [freesound.org](https://freesound.org)
  - [Eelke - tawny owl](https://freesound.org/people/Eelke/sounds/593654/) (CC BY-NC 4.0)
  - [aerror - campfire](https://freesound.org/people/aerror/sounds/350757/) (CC0)
  - [newlocknew - giant walk](https://freesound.org/people/newlocknew/sounds/628248/) (CC BY-NC 4.0)
  - [soundmast123 - dragon breathe](https://freesound.org/people/soundmast123/sounds/532156/) (CC0)
  - [Lamakin - distant monsters](https://freesound.org/people/Lamakin/sounds/443812/) (CC BY-NC 4.0)
