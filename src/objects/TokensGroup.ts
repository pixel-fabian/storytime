import Token from './Token';

export default class TokensGroup extends Phaser.Physics.Arcade.Group {
  constructor(
    world: Phaser.Physics.Arcade.World,
    scene: Phaser.Scene,
    children?:
      | Phaser.GameObjects.GameObject[]
      | Phaser.Types.GameObjects.Group.GroupConfig
      | Phaser.Types.GameObjects.Group.GroupCreateConfig,
    config?:
      | Phaser.Types.GameObjects.Group.GroupConfig
      | Phaser.Types.GameObjects.Group.GroupCreateConfig,
  ) {
    super(world, scene, children, config);
    scene.add.existing(this);
  }

  public createTokens(tokensRawData: Paragraph[] | undefined) {
    tokensRawData?.forEach((paragraph: Paragraph) => {
      const newToken = new Token(
        this.scene,
        paragraph,
        this.checkEnabled(paragraph),
      );
      this.add(newToken);
    });
  }

  public checkEnabled(
    paragraph: Paragraph,
    collectedTokens: Paragraph[] = [],
  ): boolean {
    if (paragraph.requirements.collected.length > 0) {
      // Check if all required tokens are collected
      const reqCollected = collectedTokens.filter((item: Paragraph) =>
        paragraph.requirements.collected.includes(item.id),
      );
      if (reqCollected.length != paragraph.requirements.collected.length)
        return false;
    }
    if (paragraph.requirements.notCollected.length > 0) {
      // Check if any not allowed token was collected
      const reqNotCollected = collectedTokens.filter((item: Paragraph) =>
        paragraph.requirements.collected.includes(item.id),
      );
      if (reqNotCollected.length > 0) return false;
    }
    return true;
  }

  public updateTokens(collectedTokens: Paragraph[] = []) {
    const aTokens = this.getChildren();
    aTokens.forEach((tkn) => {
      const token = tkn as Token;
      token.setEnabled(
        this.checkEnabled(token.getParagraph(), collectedTokens),
      );
    });
  }
}
