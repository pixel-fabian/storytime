export default class Token extends Phaser.Physics.Arcade.Image {
  private paragraph: Paragraph;
  private enabled!: boolean;

  constructor(scene: Phaser.Scene, paragraph: Paragraph, enabled = true) {
    super(
      scene,
      paragraph.position.x,
      paragraph.position.y,
      `storytoken_${paragraph.id}`,
    );
    scene.add.existing(this);
    scene.physics.add.existing(this);
    this.setCollideWorldBounds(true);
    this.setImmovable(true);
    this.setBounce(0);
    this.setScale(2);
    this.setBodySize(25, 25);

    this.setEnabled(enabled);
    this.paragraph = paragraph;
  }

  //////////////////////
  // GETTER & SETTER  //
  //////////////////////

  public getParagraph() {
    return this.paragraph;
  }

  public getEnabled() {
    return this.enabled;
  }

  public setEnabled(bool: boolean) {
    this.enabled = bool;

    if (this.enabled) {
      this.alpha = 1;
    } else {
      this.alpha = 0.2;
    }
  }
}
