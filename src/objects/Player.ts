import { TEXTURES } from '../constants';

export default class Player extends Phaser.Physics.Arcade.Sprite {
  private velocity: number;

  constructor(
    scene: Phaser.Scene,
    x: number,
    y: number,
    texture: string | Phaser.Textures.Texture,
    frame?: string | number,
  ) {
    super(scene, x, y, texture, frame);
    scene.add.existing(this);
    scene.physics.add.existing(this);
    this.setCollideWorldBounds(true);
    this.setScale(1.5);
    this.scene.cameras.main.startFollow(this);
    this.setPipeline('Light2D');
    this.anims.create({
      key: TEXTURES.PLAYER,
      frames: this.anims.generateFrameNumbers(TEXTURES.PLAYER, {
        start: 1,
        end: 3,
      }),
      frameRate: 6,
      repeat: -1, // -1: infinity
    });

    this.velocity = 275;
  }

  move(keys: Keys) {
    this.setVelocity(0);
    // player movement as vector to ensure same speed diagonally
    const playerDirection = new Phaser.Math.Vector2(0, 0);

    if (keys.cursors.left.isDown || keys.A.isDown) {
      playerDirection.x -= 1;
      this.flipX = true;
    } else if (keys.cursors.right.isDown || keys.D.isDown) {
      playerDirection.x += 1;
      this.flipX = false;
    }
    if (keys.cursors.up.isDown || keys.W.isDown) {
      playerDirection.y -= 1;
    } else if (keys.cursors.down.isDown || keys.S.isDown) {
      playerDirection.y += 1;
    }

    playerDirection.setLength(this.velocity);
    this.setVelocity(playerDirection.x, playerDirection.y);
    if (this.body.velocity.x === 0 && this.body.velocity.y === 0) {
      this.anims.pause();
      this.setFrame(0);
    } else if (this.velocity != 0 && this.anims.isPaused) {
      this.anims.play(TEXTURES.PLAYER);
    }
  }
}
