import 'phaser';
import LoadScene from './scenes/LoadScene';
import MenuScene from './scenes/MenuScene';
import GameScene from './scenes/GameScene';
import UiScene from './scenes/UiScene';
import IntroScene from './scenes/IntroScene';
import OutroScene from './scenes/OutroScene';

const config: Phaser.Types.Core.GameConfig = {
  type: Phaser.AUTO, // WebGL if available
  title: 'Storytime',
  width: 800,
  height: 600,
  parent: 'game',
  scene: [LoadScene, MenuScene, GameScene, UiScene, IntroScene, OutroScene],
  render: {
    pixelArt: true,
  },
  physics: {
    default: 'arcade',
    arcade: {
      debug: false,
      gravity: {
        y: 0,
      },
    },
  },
};

window.onload = () => {
  new Phaser.Game(config);
};
