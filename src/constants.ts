/* eslint-disable no-unused-vars */
export enum SCENES {
  LOAD = 'LoadScene',
  MENU = 'MenuScene',
  GAME = 'GameScene',
  UI = 'UiScene',
  INTRO = 'IntroScene',
  OUTRO = 'OutroScene',
}

export enum TEXTURES {
  MENU = 'menu',
  PLAYER = 'player',
  TOKEN = 'token',
  MAP_BG = 'map_bg',
  MAP_BLOCKED_1 = 'map_blocked_1',
  MAP_BLOCKED_2A = 'map_blocked_2a',
  MAP_BLOCKED_2B = 'map_blocked_2b',
  MAP_BLOCKED_2C = 'map_blocked_2c',
  MAP_BLOCKED_3 = 'map_blocked_3',
  MAP_BLOCKED_4 = 'map_blocked_4',
  MAP_BLOCKED_4A = 'map_blocked_4a',
  MAP_BLOCKED_4B = 'map_blocked_4b',
  MAP_BLOCKED_4C = 'map_blocked_4c',
  MAP_BLOCKED_4D = 'map_blocked_4d',
  MAP_FG = 'map_fg',
}

export enum AUDIO {
  CAMPFIRE = 'campfire',
  DRAGON = 'dragon',
  DRAGON_DISTANT = 'dragon_distant',
  DRAGON_WALK = 'dragon_walk',
  FIGHT = 'fight',
  OWL = 'tawny_owl',
  SOUNDTRACK = 'soundtrack',
  POWERUP = 'powerup',
}

export enum TEXTSTYLE {
  FONT = 'Immortal',
}

export enum EVENTS {
  TIMEUP = 'timeup',
  COLLECT_TOKEN = 'collect_token',
}

export const COLORS = {
  BACKGROUND: new Phaser.Display.Color(18, 3, 48, 255),
  TEXT: new Phaser.Display.Color(255, 255, 255, 255),
  PRIMARY: new Phaser.Display.Color(242, 19, 183, 255),
};
