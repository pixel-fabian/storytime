export default class Helper {
  /**
   * Create random coordinates within the scene
   *
   * @param scene Phaser scene
   * @returns
   */
  static createRandomCoords(scene: Phaser.Scene) {
    // width and height of the game screen
    const width = scene.scale.width;
    const height = scene.scale.height;

    const x = Phaser.Math.Between(16, width - 16);
    const y = Phaser.Math.Between(16, height - 16);

    return { x, y };
  }

  /**
   * Add text char by char (Typewriter effect)
   *
   * @param scene Phaser scene
   * @param textObj Text object
   * @param sText String of text to display
   */
  static textTypewriter(
    scene: Phaser.Scene,
    textObj: Phaser.GameObjects.Text,
    sText: string,
  ) {
    const length = sText.length;
    let i = 0;
    scene.time.addEvent({
      callback: () => {
        textObj.text += sText[i];
        ++i;
      },
      repeat: length - 1,
      delay: 50,
    });
  }
}
