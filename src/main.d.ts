/* eslint-disable no-unused-vars */
interface Keys {
  cursors: Phaser.Types.Input.Keyboard.CursorKeys;
  W: Phaser.Input.Keyboard.Key;
  A: Phaser.Input.Keyboard.Key;
  S: Phaser.Input.Keyboard.Key;
  D: Phaser.Input.Keyboard.Key;
}

interface Paragraph {
  id: number;
  name: string;
  position: {
    x: number;
    y: number;
  };
  requirements: {
    collected: number[];
    notCollected: number[];
    replaces: number[];
  };
  text: string[];
  text_drunk: string[];
}
