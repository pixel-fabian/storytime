import { AUDIO, SCENES, TEXTURES } from '../constants';
import TextButton from '../objects/TextButton';

export default class MenuScene extends Phaser.Scene {
  private soundCampfire?: Phaser.Sound.BaseSound;
  private soundOwl?: Phaser.Sound.BaseSound;

  constructor() {
    super({
      key: SCENES.MENU,
    });
  }

  //////////////////////////////////////////////////
  // LIFECYCLE (init, preload, create, update)    //
  //////////////////////////////////////////////////

  init(): void {}

  preload(): void {}

  create(): void {
    const background = this.add.sprite(0, 0, TEXTURES.MENU).setOrigin(0);
    this.anims.create({
      key: TEXTURES.MENU,
      frames: this.anims.generateFrameNumbers(TEXTURES.MENU, {
        start: 0,
        end: 3,
      }),
      frameRate: 4,
      repeat: -1, // -1: infinity
    });
    background.play(TEXTURES.MENU);
    this.createButtons();
    this.soundCampfire = this.sound.add(AUDIO.CAMPFIRE);
    this.soundOwl = this.sound.add(AUDIO.OWL);

    this.soundCampfire.play({ loop: true });
    this.soundOwl.play({ loop: true });
  }

  update(): void {}

  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////

  private createButtons() {
    new TextButton(
      this,
      410,
      100,
      ['Let me tell you the story of how', 'I defeated the mighty dragon...'],
      {
        fontFamily: 'Immortal',
        color: '#10141f',
        fontSize: '1.4rem',
      },
      '#411d31',
      () => {
        this.soundCampfire?.stop();
        this.soundOwl?.stop();
        this.scene.start(SCENES.INTRO);
      },
    );
  }
}
