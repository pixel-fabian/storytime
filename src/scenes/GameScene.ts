import { AUDIO, EVENTS, SCENES, TEXTURES } from '../constants';
import Player from '../objects/Player';
import Token from '../objects/Token';
import Tokens from '../objects/TokensGroup';
import tokensStoreInstance from '../stores/TokensStore';

export default class GameScene extends Phaser.Scene {
  public player!: Player;
  private keys!: Keys;
  private light!: Phaser.GameObjects.Light;
  private tokens!: Tokens;
  private blocking!: Phaser.Physics.Arcade.StaticGroup;
  private soundPickup?: Phaser.Sound.BaseSound;

  constructor() {
    super({
      key: SCENES.GAME,
    });
  }

  //////////////////////////////////////////////////
  // LIFECYCLE (init, preload, create, update)    //
  //////////////////////////////////////////////////

  init(): void {}

  preload(): void {}

  create() {
    this.sound.add(AUDIO.SOUNDTRACK).play();
    this.createMap();
    this.scene.launch(SCENES.UI);
    this.createControls();

    this.player = new Player(this, 960, 1400, TEXTURES.PLAYER, 0);
    this.player.setPipeline('Light2D');
    this.physics.add.collider(this.blocking, this.player);

    this.light = this.lights.addLight(
      this.player.x,
      this.player.y,
      200,
      0xffaa89,
    );
    this.createLights();

    this.tokens = new Tokens(this.physics.world, this);
    this.tokens.createTokens(tokensStoreInstance.getTokenRawData());
    this.physics.add.collider(
      this.tokens,
      this.player,
      undefined,
      this.collisionTokenPlayer,
      this,
    );
    this.soundPickup = this.sound.add(AUDIO.POWERUP);
    // foreground
    this.add.image(0, 0, TEXTURES.MAP_FG).setOrigin(0, 0);
  }

  update(): void {
    this.player?.move(this.keys);
    this.followPlayer();
  }

  //////////////////////////////////////////////////
  // Public methods                               //
  //////////////////////////////////////////////////

  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////

  private createControls() {
    this.keys = {
      cursors: this.input.keyboard.createCursorKeys(),
      W: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W),
      A: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A),
      S: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S),
      D: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D),
    };
  }

  private createLights() {
    this.lights.enable().setAmbientColor(0x666666);

    this.lights.addLight(960, 1400, 200, 0xffffff, 1);

    this.lights.addLight(600, 1300, 300, 0xffffff, 1.5);
    this.lights.addLight(400, 1350, 200, 0xffffff, 1);
    this.lights.addLight(400, 1150, 200, 0xffffff, 1);

    this.lights.addLight(1300, 200, 200, 0xffffff, 1);
  }

  private createMap() {
    // background
    const background = this.add.image(0, 0, TEXTURES.MAP_BG).setOrigin(0, 0);
    background.setPipeline('Light2D');
    this.physics.world.bounds.width = background.width;
    this.physics.world.bounds.height = background.height;
    this.cameras.main.setBounds(0, 0, background.width, background.height);
    // blocking elements
    this.blocking = this.physics.add.staticGroup();
    this.blocking.create(695, 1780, TEXTURES.MAP_BLOCKED_1);
    this.blocking.create(1060, 80, TEXTURES.MAP_BLOCKED_2A);
    this.blocking.create(1810, 225, TEXTURES.MAP_BLOCKED_2B);
    this.blocking.create(2300, 50, TEXTURES.MAP_BLOCKED_2C);
    this.blocking.create(2138, 1020, TEXTURES.MAP_BLOCKED_3);
    this.blocking.create(1835, 1650, TEXTURES.MAP_BLOCKED_4);
    this.blocking.create(1570, 2015, TEXTURES.MAP_BLOCKED_4A);
    this.blocking.create(2020, 2070, TEXTURES.MAP_BLOCKED_4B);
    this.blocking.create(2745, 2240, TEXTURES.MAP_BLOCKED_4C);
    this.blocking.create(3050, 1535, TEXTURES.MAP_BLOCKED_4D);
    this.blocking.getChildren().forEach((cld) => {
      const child = cld as Phaser.Physics.Arcade.Image;
      child.setPipeline('Light2D');
    });
  }

  private followPlayer() {
    this.light.x = this.player.x;
    this.light.y = this.player.y;
  }

  private collisionTokenPlayer(
    plyr: Phaser.GameObjects.GameObject,
    tkn: Phaser.GameObjects.GameObject,
  ) {
    const token = tkn as Token;
    if (!token.getEnabled()) return false;

    this.soundPickup?.play();
    this.events.emit(EVENTS.COLLECT_TOKEN, token.getParagraph());
    token.destroy();
    return true;
  }

  getTokens() {
    return this.tokens;
  }
}
