import { AUDIO, SCENES, TEXTSTYLE } from '../constants';
import TextButton from '../objects/TextButton';
import Helper from '../utils/Helper';

export default class IntroScene extends Phaser.Scene {
  constructor() {
    super({
      key: SCENES.INTRO,
    });
  }

  //////////////////////////////////////////////////
  // LIFECYCLE (init, preload, create, update)    //
  //////////////////////////////////////////////////

  init(): void {}

  preload(): void {}

  create(): void {
    this.sound.add(AUDIO.DRAGON_DISTANT).play();

    this.createIntroTexts();
    this.createButton();
  }

  update(): void {}

  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////

  private createIntroTexts() {
    const introText1 = this.add
      .text(this.scale.width / 2, 200, '', {
        fontFamily: TEXTSTYLE.FONT,
        color: '#fff',
        fontSize: '1.5rem',
      })
      .setOrigin(0.5, 1);
    Helper.textTypewriter(
      this,
      introText1,
      'The dragon was approaching our lands.',
    );
    const introText2 = this.add
      .text(this.scale.width / 2, 250, '', {
        fontFamily: TEXTSTYLE.FONT,
        color: '#fff',
        fontSize: '1.5rem',
      })
      .setOrigin(0.5, 1);

    // start second sentance after first one is finished
    this.time.addEvent({
      delay: 38 * 50,
      callback: () => {
        Helper.textTypewriter(
          this,
          introText2,
          'I had only seconds to prepare!',
        );
      },
      loop: false,
    });
  }

  private createButton() {
    new TextButton(
      this,
      this.scale.width / 2,
      400,
      ['Start'],
      {
        fontFamily: 'Immortal',
        color: '#fff',
        fontSize: '1.5rem',
      },
      '#eee',
      () => {
        this.scene.start(SCENES.GAME);
      },
    ).setOrigin(0.5);
  }
}
