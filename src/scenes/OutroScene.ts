import { AUDIO, SCENES, TEXTSTYLE } from '../constants';
import TextButton from '../objects/TextButton';

export default class OutroScene extends Phaser.Scene {
  private soundDragonWalk?: Phaser.Sound.BaseSound;
  private soundDragon?: Phaser.Sound.BaseSound;
  killConditions = [[0], [81], [82], [42, 80]];

  constructor() {
    super({
      key: SCENES.OUTRO,
    });
  }

  //////////////////////////////////////////////////
  // LIFECYCLE (init, preload, create, update)    //
  //////////////////////////////////////////////////

  init(): void {}

  preload(): void {}

  create(collectedTokens: Paragraph[]): void {
    this.createSounds();
    this.createButton();
    this.createOutroTexts(collectedTokens);
  }

  update(): void {}

  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////

  private createSounds() {
    this.soundDragon = this.sound.add(AUDIO.DRAGON);
    this.soundDragonWalk = this.sound.add(AUDIO.DRAGON_WALK);
    this.soundDragonWalk.play();
    this.soundDragonWalk.on('complete', () => {
      this.soundDragon?.play();
    });
  }

  private async createOutroTexts(collectedTokens: Paragraph[]) {
    const text: string[] = [];
    collectedTokens.sort((a, b) => {
      return a.id - b.id;
    });

    collectedTokens.forEach((paragraph: Paragraph) => {
      paragraph.text.forEach((item) => {
        text.push(item);
      });
    });

    if (this.isKilled(collectedTokens)) {
      text.push(
        'And this is how I defeated the mighty dragon once and for all.',
      );
    } else {
      text.push('Ähhm.. ...well.. and then.. then the dragon flew away.');
      text.push('So I defeated the beast, right?');
    }

    this.add
      .text(this.scale.width / 2, this.scale.height / 2 - 100, text, {
        fontFamily: TEXTSTYLE.FONT,
        color: '#fff',
        fontSize: '1rem',
        align: 'center',
      })
      .setOrigin(0.5);
  }

  private createButton() {
    new TextButton(
      this,
      this.scale.width,
      0,
      ['X'],
      {
        fontFamily: 'Immortal',
        color: '#fff',
        fontSize: '1.5rem',
      },
      '#eee',
      () => {
        this.soundDragon?.stop();
        this.soundDragonWalk?.stop();
        this.scene.start(SCENES.MENU);
      },
    ).setOrigin(1, 0);
  }

  private isKilled(collectedTokens: Paragraph[]) {
    let killed = false;
    if (collectedTokens.length > 0) {
      this.killConditions.forEach((requiredTokens) => {
        // Check if all required are collected
        const reqCollected = collectedTokens.filter((item: Paragraph) =>
          requiredTokens.includes(item.id),
        );
        if (reqCollected.length === requiredTokens.length) killed = true;
      });
    }

    return killed;
  }
}
