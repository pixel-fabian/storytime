import { SCENES, TEXTURES, AUDIO, TEXTSTYLE } from '../constants';
import LoadingBar from '../objects/LoadingBar';
import tokensStoreInstance from '../stores/TokensStore';

export default class LoadScene extends Phaser.Scene {
  constructor() {
    super({
      key: SCENES.LOAD,
    });
  }

  //////////////////////////////////////////////////
  // LIFECYCLE (init, preload, create, update)    //
  //////////////////////////////////////////////////

  init(): void {}

  async preload() {
    // add text
    this.add.text(360, 225, 'Loading...', {
      fontFamily: TEXTSTYLE.FONT,
      color: '#fff',
    });
    // create loading bar
    const loadingBar = new LoadingBar(this, 255, 255, 300, 40);
    this.load.on('progress', (percentage: number) => {
      loadingBar.fillBar(percentage);
    });
    // load all textures
    this.load.image(TEXTURES.MAP_BG, 'assets/img/map_background1.png');
    this.load.image(TEXTURES.MAP_FG, 'assets/img/map_foreground2.png');
    this.load.image(TEXTURES.MAP_BLOCKED_1, 'assets/img/map_blocked_1.png');
    this.load.image(TEXTURES.MAP_BLOCKED_2A, 'assets/img/map_blocked_2a.png');
    this.load.image(TEXTURES.MAP_BLOCKED_2B, 'assets/img/map_blocked_2b.png');
    this.load.image(TEXTURES.MAP_BLOCKED_2C, 'assets/img/map_blocked_2c.png');
    this.load.image(TEXTURES.MAP_BLOCKED_3, 'assets/img/map_blocked_3.png');
    this.load.image(TEXTURES.MAP_BLOCKED_4, 'assets/img/map_blocked_4.png');
    this.load.image(TEXTURES.MAP_BLOCKED_4A, 'assets/img/map_blocked_4a.png');
    this.load.image(TEXTURES.MAP_BLOCKED_4B, 'assets/img/map_blocked_4b.png');
    this.load.image(TEXTURES.MAP_BLOCKED_4C, 'assets/img/map_blocked_4c.png');
    this.load.image(TEXTURES.MAP_BLOCKED_4D, 'assets/img/map_blocked_4d.png');
    this.load.spritesheet(TEXTURES.MENU, 'assets/img/campfire_forrest.png', {
      frameWidth: 800,
      frameHeight: 600,
    });
    this.load.spritesheet(TEXTURES.PLAYER, 'assets/img/player.png', {
      frameWidth: 32,
      frameHeight: 64,
    });
    const tokensStore = tokensStoreInstance;
    await tokensStore.update();
    tokensStore.getTokenRawData()?.forEach((paragraph: Paragraph) => {
      this.load.spritesheet(
        `storytoken_${paragraph.id}`,
        `assets/img/storytoken_${paragraph.id}.png`,
        {
          frameWidth: 32,
          frameHeight: 32,
        },
      );
    });

    // load all audio
    this.load.audio(AUDIO.CAMPFIRE, ['assets/sfx/campfire.wav']);
    this.load.audio(AUDIO.DRAGON, ['assets/sfx/dragon-breathe-ire.wav']);
    this.load.audio(AUDIO.DRAGON_DISTANT, ['assets/sfx/dragon-distant2.wav']);
    this.load.audio(AUDIO.DRAGON_WALK, ['assets/sfx/dragon-walk.wav']);
    this.load.audio(AUDIO.OWL, ['assets/sfx/tawny-owl.wav']);
    this.load.audio(AUDIO.SOUNDTRACK, ['assets/music/explore_and_equip.wav']);
    this.load.audio(AUDIO.POWERUP, ['assets/sfx/powerUp.wav']);
  }

  create(): void {
    this.scene.start(SCENES.MENU);
  }

  update(): void {}

  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////
}
