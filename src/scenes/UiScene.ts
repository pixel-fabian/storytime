import { EVENTS, SCENES } from '../constants';
import Token from '../objects/Token';
import GameScene from './GameScene';

export default class UiScene extends Phaser.Scene {
  private gameScene!: Phaser.Scene;
  private timer!: number;
  private collectedTokens!: Paragraph[];

  constructor() {
    super({
      key: SCENES.UI,
    });
  }

  //////////////////////////////////////////////////
  // LIFECYCLE (init, preload, create, update)    //
  //////////////////////////////////////////////////

  init(): void {
    this.gameScene = this.scene.get(SCENES.GAME);
  }

  preload(): void {}

  create(): void {
    this.timer = 21;
    this.collectedTokens = [];
    this.createUi();
    this.setupEvents();

    const gameScene = this.gameScene as GameScene;
    const tokenNaked = gameScene.getTokens().getChildren()[0] as Token;
    this.collectToken(tokenNaked.getParagraph());
  }

  update(): void {}

  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////

  private createUi() {
    this.time.addEvent({
      callback: () => {
        this.timer--;

        if (this.timer === 0) {
          this.events.emit(EVENTS.TIMEUP);
          this.cameras.main.fadeOut(1000, 0, 0, 0);
        }
      },
      repeat: this.timer - 1,
      delay: 1000,
    });
  }

  private setupEvents() {
    this.cameras.main.once(
      Phaser.Cameras.Scene2D.Events.FADE_OUT_COMPLETE,
      () => {
        this.scene.stop();
        this.scene.stop(SCENES.GAME);
        this.scene.start(SCENES.OUTRO, this.collectedTokens);
      },
    );

    if (!this.gameScene.events.eventNames().includes(EVENTS.COLLECT_TOKEN)) {
      this.gameScene.events.on(EVENTS.COLLECT_TOKEN, (paragraph: Paragraph) => {
        this.collectToken(paragraph);
      });
    }
  }

  private collectToken(paragraph: Paragraph) {
    if (this.collectedTokens.includes(paragraph)) return;

    if (paragraph.requirements.replaces.length > 0) {
      paragraph.requirements.replaces.forEach((replaceId) => {
        this.collectedTokens = this.collectedTokens.filter(
          (token) => replaceId != token.id,
        );
      });
    }
    this.collectedTokens.push(paragraph);

    const gameScene = this.gameScene as GameScene;
    gameScene.getTokens().updateTokens(this.collectedTokens);
  }
}
