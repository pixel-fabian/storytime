let instance: TokensStore;

class TokensStore {
  private tokenRawData?: Paragraph[];

  constructor() {
    if (instance) {
      throw new Error('New instance cannot be created!!');
    }
    instance = this;
  }

  public async update() {
    await fetch('assets/text/storytokens.json')
      .then((response) => response.json())
      .then((paragraphs) => {
        this.tokenRawData = paragraphs;
      });
  }

  public getTokenRawData() {
    return this.tokenRawData;
  }
}

const tokensStoreInstance = new TokensStore();
export default tokensStoreInstance;
